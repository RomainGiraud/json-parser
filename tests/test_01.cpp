#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "JsonParser.h"

#include <iostream>
using namespace std;

SCENARIO("parse from file", "[load-file]")
{
    GIVEN("a json parser")
    {
        JsonParser parser;

        WHEN("file exists and content is valid")
        {
            json_value* v = parser.parseFile(TEST_PATH "/test-01.json");

            THEN("return value is a correct json_value")
            {
                REQUIRE(v != nullptr);
            }

            delete v;
        }

        WHEN("file doest not exist")
        {
            THEN("an error occured")
            {
                REQUIRE_THROWS_AS(parser.parseFile(TEST_PATH "/test-01"), std::runtime_error);
            }
        }
    }

    GIVEN("a json parser")
    {
        JsonParser parser;

        WHEN("files are identical")
        {
            json_value* v1 = parser.parseFile(TEST_PATH "/test-01.json");
            json_value* v2 = parser.parseFile(TEST_PATH "/test-01.json");

            THEN("stringify values are the same")
            {
                std::string s1 = boost::apply_visitor(json_to_string(), *v1);
                std::string s2 = boost::apply_visitor(json_to_string(), *v2);
                REQUIRE(s1 == s2);
            }

            THEN("binary representation are equals")
            {
                REQUIRE(boost::apply_visitor(json_equals(), *v1, *v2));
            }

            delete v1;
            delete v2;
        }

        WHEN("files are not identical")
        {
            json_value* v1 = parser.parseFile(TEST_PATH "/test-01.json");
            json_value* v2 = parser.parseFile(TEST_PATH "/test-02.json");

            THEN("stringify values are not the same")
            {
                std::string s1 = boost::apply_visitor(json_to_string(), *v1);
                std::string s2 = boost::apply_visitor(json_to_string(), *v2);
                REQUIRE(s1 != s2);
            }

            THEN("binary representation are not equals")
            {
                REQUIRE(!boost::apply_visitor(json_equals(), *v1, *v2));
            }

            delete v1;
            delete v2;
        }
    }
}


SCENARIO("parse from string", "[string]")
{
    GIVEN("a json parser")
    {
        JsonParser parser;

        WHEN("string is valid")
        {
            json_value* v = parser.parseString("[1, 2, 3]");

            THEN("return value is a correct json_value")
            {
                REQUIRE(v != nullptr);
            }

            delete v;
        }

        WHEN("string is invalid")
        {
            THEN("an error occured")
            {
                REQUIRE_THROWS_AS(parser.parseString("[1, , 3]"), std::runtime_error);
            }
        }
    }
}