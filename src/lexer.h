#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <fstream>

#include "Driver.h"

class Scanner
{ 
public:
    explicit Scanner(std::istream& ifs_, size_t initSize = 1024);
    explicit Scanner(const std::string& str);
    ~Scanner();

    bool fill(int n);

    std::string text();
    int length();
    int lineno();
    void increment_line_number();

    int scan(YYSTYPE& yylval);

private:
    // iostream sucks. very slow.
    std::istream* _ifs;

    // buffer memory
    char*  _buffer;
    size_t _bufferSize;

    // current position
    char* _cursor;
    char* _limit;
    char* _token;
    char* _marker;
    int _lineno;
};

#endif // LEXER_H
