%include
{
#include <cassert>
#include <iostream>

#include "Driver.h"

using namespace std;
}

%syntax_error
{
    state->error_found = true;
    throw std::runtime_error("Error parsing command");
    //assert(0);
}

%parse_failure {
    throw std::runtime_error("Giving up. Parser is hopelessly lost...");
}

%default_destructor {
    (void) state;
}

%extra_argument { ParserState* state }

%token_prefix       TOKEN_
%token_type         { TokenValue }
%type object        { json_object* }
%type pairList      { json_object* }
%type array         { json_array* }
%type valueList     { json_array* }
%type value         { json_value* }


doc ::= value(v) .
{
    state->json = v;
}

object(o) ::= BRACE_OPEN BRACE_CLOSE .
{
    o = new json_object;
}
object(o) ::= BRACE_OPEN pairList(l) BRACE_CLOSE .
{
    o = l;
}

pairList(l) ::= STRING(s) COLON value(v) .
{
    l = new json_object;
    (*l)[string(s.str, s.str_len)] = *v;
    free(s.str);
    delete v;
}
pairList(l) ::= STRING(s) COLON value(v) COMMA pairList(ll) .
{
    l = ll;
    (*l)[string(s.str, s.str_len)] = *v;
    free(s.str);
    delete v;
}

array(a) ::= BRACKET_OPEN BRACKET_CLOSE .
{
    a = new json_array;
}
array(a) ::= BRACKET_OPEN valueList(l) BRACKET_CLOSE .
{
    a = l;
}

valueList(l) ::= value(v) .
{
    l = new json_array;
    l->push_back(*v);
    delete v;
}
valueList(l) ::= value(v) COMMA valueList(ll) .
{
    l = ll;
    ll->push_front(*v);
    delete v;
}

value(v) ::= STRING(s) .
{
    v = new json_value(string(s.str, s.str_len));
    free(s.str);
}
value(v) ::= NUMBER_INT(n) .
{
    v = new json_value(n.numberi);
}
value(v) ::= NUMBER_FLOAT(n) .
{
    v = new json_value(n.numberf);
}
value(v) ::= BOOLEAN(b) .
{
    v = new json_value(b.boolean);
}
value(v) ::= NULL .
{
    v = new json_value(nullptr);
}
value(v) ::= array(a) .
{
    v = new json_value(*a);
    delete a;
}
value(v) ::= object(o) .
{
    v = new json_value(*o);
    delete o;
}
