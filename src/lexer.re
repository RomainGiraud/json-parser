#include "lexer.h"

#include <iostream>

#include "parser.h"

using namespace std;

/*!max:re2c*/

Scanner::Scanner(std::istream& ifs, size_t initSize)
    : _ifs(&ifs)
    , _buffer(0)
    , _bufferSize(initSize)
    , _cursor(0)
    , _limit(0)
    , _token(0)
    , _marker(0)
    , _lineno(1)
{
    _buffer = new char[_bufferSize];
    _cursor = _limit = _token = _marker = _buffer;
}

Scanner::Scanner(const std::string& str)
    : _ifs(0)
    , _buffer(0)
    , _bufferSize(str.size() + YYMAXFILL)
    , _cursor(0)
    , _limit(0)
    , _token(0)
    , _marker(0)
    , _lineno(1)
{
    _buffer = new char[_bufferSize];
    memcpy(_buffer, str.c_str(), str.size());
    memset(_buffer + str.size(), ' ', YYMAXFILL);

    _cursor = _buffer;
    _token  = _buffer;
    _marker = _buffer;
    _limit  = _buffer + _bufferSize;
}

Scanner::~Scanner()
{
    delete [] _buffer;
}

bool Scanner::fill(int n)
{
    if (_ifs == 0) return _limit - _cursor > 0;

    // is eof?
    if (_ifs->eof())
    {
        if ((_limit - _cursor) <= 0)
        {
            return false;
        }
    }

    int restSize = _limit - _token;
    if (restSize + n >= _bufferSize)
    {
        // extend buffer
        _bufferSize *= 2;
        char* newBuffer = new char[_bufferSize];
        // memcpy
        for (int i = 0; i < restSize; ++i)
        {
            *(newBuffer + i) = *(_token + i);
        }
        _cursor = newBuffer + (_cursor - _token);
        _token = newBuffer;
        _limit = newBuffer + restSize;

        delete [] _buffer;
        _buffer = newBuffer;
    }
    else
    {
        // move remained data to head.
        //memmove( m_buffer, m_token, (restSize)*sizeof(char) );
        for (int i = 0; i < restSize; ++i)
        {
            *(_buffer + i) = *(_token + i);
        }
        _cursor = _buffer + (_cursor - _token);
        _token = _buffer;
        _limit = _buffer + restSize;
    }

    // fill to buffer
    int readSize = _bufferSize - restSize;
    _ifs->read(_limit, readSize);
    _limit += _ifs->gcount();

    return true;
}

std::string Scanner::text()
{
    return std::string(_token, _token + length());
}

int Scanner::length()
{
    return (_cursor - _token);
}

int Scanner::lineno()
{
    return _lineno;
}

void Scanner::increment_line_number()
{
    _lineno++;
}

int Scanner::scan(YYSTYPE& yylval)
{
std:
    _token = _cursor;

/*!re2c
    re2c:define:YYCTYPE = "char";
    re2c:define:YYCURSOR = _cursor;
    re2c:define:YYMARKER = _marker;
    re2c:define:YYLIMIT = _limit;
    re2c:define:YYFILL:naked = 1;
    re2c:define:YYFILL@len = #;
    re2c:define:YYFILL = "if (!fill(#)) { return 0; }";
    re2c:yyfill:enable = 1;
    re2c:indent:top = 2;
    re2c:indent:string="    ";

    quote       = "\\" "\"";
    solidus     = "\\" "\\";
    rsolidus    = "\\" "/";
    backspace   = "\\" "b";
    formfeed    = "\\" "f";
    newline     = "\\" "n";
    return      = "\\" "r";
    tab         = "\\" "t";
    unicode     = "\\" [0-9a-fA-F]{4};
    character   = [^"\\] | quote | solidus | rsolidus | backspace | formfeed | newline | return | tab | unicode;

    int         = "-"? ("0"|[1-9][0-9]*);
    float       = int "." [0-9]+ ( [eE] [+-]? [0-9]+ )?;


    "{"                     { return TOKEN_BRACE_OPEN; }
    "}"                     { return TOKEN_BRACE_CLOSE; }

    "["                     { return TOKEN_BRACKET_OPEN; }
    "]"                     { return TOKEN_BRACKET_CLOSE; }

    ","                     { return TOKEN_COMMA; }
    ":"                     { return TOKEN_COLON; }

    "true"                  { yylval.boolean = true; return TOKEN_BOOLEAN; }
    "false"                 { yylval.boolean = false; return TOKEN_BOOLEAN; }
    "null"                  { return TOKEN_NULL; }

    "\"" character* "\""    { yylval.str_len = length(); yylval.str = strndup(_token, yylval.str_len); return TOKEN_STRING; }
    int                     { yylval.numberi = std::atoi(_token); return TOKEN_NUMBER_INT; }
    float                   { yylval.numberf = std::atof(_token); return TOKEN_NUMBER_FLOAT; }

    [ \r\n\t\f]             { goto std; }
    .                       { cerr << "[error] unknown character: " << text() << endl; return -1; }
*/
}
