#include "JsonParser.h"

using namespace std;

JsonParser::JsonParser()
{
}

void JsonParser::trace(bool trace)
{
	if (trace)
		ParseTrace(stdout, (char*)"");
	else
		ParseTrace(0, 0);
}

json_value* JsonParser::parseFile(const std::string& filename)
{
    ifstream file(filename);
    if (!file.is_open())
    {
        throw runtime_error("error during opening file " + filename);
    }

    // Set up the scanner
    TokenValue yylval;
    Scanner scanner(file);
 
    // Set up the parser
    ParserState state;
    void* shellParser = ParseAlloc(malloc);

    int lexCode;
    while (true)
    {
        lexCode = scanner.scan(yylval);
        Parse(shellParser, lexCode, yylval, &state);

        if (lexCode <= 0 || state.error_found)
            break;
    }

    if (lexCode == -1)
    {
        delete state.json;
        state.json = nullptr;
        throw runtime_error("The scanner encountered an error.");
        //cerr << "The scanner encountered an error at line " << yyget_lineno(scanner) << " and column " << yyget_column(scanner) << "." << endl;
    }
    if (state.error_found)
    {
        delete state.json;
        state.json = nullptr;
        throw runtime_error("The parser encountered an error.");
        //cerr << "The parser encountered an error at line " << yyget_lineno(scanner) << " and column " << yyget_column(scanner) << "." << endl;
    }
 
    // Cleanup the scanner and parser
    file.close();
    ParseFree(shellParser, free);

    return state.json;
}

json_value* JsonParser::parseString(const std::string& str)
{
    // Set up the scanner
    TokenValue yylval;
    Scanner scanner(str);
 
    // Set up the parser
    ParserState state;
    void* shellParser = ParseAlloc(malloc);

    int lexCode;
    while (true)
    {
        lexCode = scanner.scan(yylval);
        Parse(shellParser, lexCode, yylval, &state);

        if (lexCode <= 0 || state.error_found)
            break;
    }

    if (lexCode == -1)
    {
        delete state.json;
        state.json = nullptr;
        throw runtime_error("The scanner encountered an error.");
        //cerr << "The scanner encountered an error at line " << yyget_lineno(scanner) << " and column " << yyget_column(scanner) << "." << endl;
    }
    if (state.error_found)
    {
        delete state.json;
        state.json = nullptr;
        throw runtime_error("The parser encountered an error.");
        //cerr << "The parser encountered an error at line " << yyget_lineno(scanner) << " and column " << yyget_column(scanner) << "." << endl;
    }
 
    // Cleanup the scanner and parser
    ParseFree(shellParser, free);

    return state.json;
}
