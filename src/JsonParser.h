#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <string>

#include "Driver.h"
#include "parser.h"
#include "lexer.h"

class JsonParser
{
public:
    JsonParser();

    void trace(bool trace);

    json_value* parseFile(const std::string& filename);
    json_value* parseString(const std::string& str);
};

#endif // JSONPARSER_H