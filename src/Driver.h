#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <list>
#include <map>
#include <iostream>
#include <sstream>

#include <boost/variant.hpp>

typedef boost::make_recursive_variant<
    bool,
    int,
    float,
    std::nullptr_t,
    std::string,
    std::list<boost::recursive_variant_>,
    std::map<std::string, boost::recursive_variant_>
>::type json_value;

typedef std::list<json_value> json_array;

typedef std::map<std::string, json_value> json_object;

struct TokenValue {
    char* str;
    size_t str_len;
    int numberi;
    float numberf;
    bool boolean;
};

typedef TokenValue YYSTYPE;

class json_to_string : public boost::static_visitor<std::string>
{
public:
    std::string operator()(bool& b) const
    {
        return (b ? "true" : "false");
    }

    std::string operator()(int& i) const
    {
        return std::to_string(i);
    }

    std::string operator()(float& f) const
    {
        return std::to_string(f);
    }

    std::string operator()(std::nullptr_t) const
    {
        return "null";
    }

    std::string operator()(std::string& s) const
    {
        return s;
    }

    std::string operator()(json_array& a) const
    {
        std::ostringstream os;
        os << '[';
        for (json_array::iterator it = a.begin(); it != a.end(); ++it)
        {
            os << boost::apply_visitor(*this, *it);
            if (it != (--a.end()))
                os << ", ";
        }
        os << ']';

        return os.str();
    }

    std::string operator()(json_object& o) const
    {
        std::ostringstream os;
        os << '{';
        for (json_object::iterator it = o.begin(); it != o.end(); ++it)
        {
            os << it->first << ": ";
            os << boost::apply_visitor(*this, it->second);
            if (it != (--o.end()))
                os << ", ";
        }
        os << '}';

        return os.str();
    }
};

class json_printer : public boost::static_visitor<>
{
public:
    void operator()(bool& b) const
    {
        std::cout << (b ? "true" : "false");
    }

    void operator()(int& i) const
    {
        std::cout << i;
    }

    void operator()(float& f) const
    {
        std::cout << f;
    }

    void operator()(std::nullptr_t) const
    {
        std::cout << "null";
    }

    void operator()(std::string& s) const
    {
        std::cout << s;
    }

    void operator()(json_array& a) const
    {
        std::cout << '[';
        for (json_array::iterator it = a.begin(); it != a.end(); ++it)
        {
            boost::apply_visitor(*this, *it);
            if (it != (--a.end()))
                std::cout << ", ";
        }
        std::cout << ']';
    }

    void operator()(json_object& o) const
    {
        std::cout << '{';
        for (json_object::iterator it = o.begin(); it != o.end(); ++it)
        {
            std::cout << it->first << ": ";
            boost::apply_visitor(*this, it->second);
            if (it != (--o.end()))
                std::cout << ", ";
        }
        std::cout << '}';
    }
};

class json_equals : public boost::static_visitor<bool>
{
public:
    template <typename T, typename U>
    bool operator()( const T &, const U & ) const
    {
        return false; // cannot compare different types
    }

    template <typename T>
    bool operator()( const T & lhs, const T & rhs ) const
    {
        return lhs == rhs;
    }
};

struct ParserState {
    json_value* json;
    bool error_found;

    ParserState()
        : json(nullptr), error_found(false)
    {}
};

void* ParseAlloc(void* (*allocProc)(size_t));
void Parse(void* parser, int token, TokenValue value, ParserState* state);
void ParseFree(void*, void(*freeProc)(void*));
void ParseTrace(FILE *stream, char *zPrefix);

#endif // DRIVER_H
